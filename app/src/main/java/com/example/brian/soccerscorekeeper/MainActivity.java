package com.example.brian.soccerscorekeeper;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    int scoreA, scoreB, foulA, foulB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resetFoulAndScore();

    }

    //A Team

    public void displayScoreA() {
        TextView view = findViewById(R.id.scoreA);
        view.setText("Score : " + this.scoreA);
    }

    public void displayFoulA() {
        TextView view = findViewById(R.id.foulA);
        view.setText("Foul : " + this.foulA);
    }

    public void onGoalClickA(View view) {
        scoreA++;
        displayScoreA();
    }

    public void onFoulClickA(View view) {
        foulA++;
        displayFoulA();
    }

    //TeamB

    public void displayScoreB() {
        TextView view = findViewById(R.id.scoreB);
        view.setText("Score : " + this.scoreB);
    }

    public void displayFoulB() {
        TextView view = findViewById(R.id.foulB);
        view.setText("Foul : " + this.foulB);
    }

    public void onGoalClickB(View view) {
        scoreB++;
        displayScoreB();
    }

    public void onFoulClickB(View view) {
        foulB++;
        displayFoulB();
    }

    //Reset method
    public void resetFoulAndScore(View view) {
        scoreA = 0;
        scoreB = 0;
        foulA = 0;
        foulB = 0;
        displayScoreB();
        displayFoulA();
        displayScoreA();
        displayFoulB();
    }

    public void resetFoulAndScore() {
        scoreA = 0;
        scoreB = 0;
        foulA = 0;
        foulB = 0;
        displayScoreB();
        displayFoulA();
        displayScoreA();
        displayFoulB();
    }
}
